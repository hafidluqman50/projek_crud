<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/pass',function(){
	dd(bcrypt('admin3'));
});

Route::get('/mantul',['uses'=>'ApiController@index']);

// ROUTE LOGIN GUEST //
Route::group(['middleware' => 'guest.has.login'],function(){
	Route::get('/',['as'=>'login-form','uses'=>'Guest\AuthController@loginForm']);
	Route::post('/login',['as'=>'login-post','uses'=>'Guest\AuthController@loginPost']);
	Route::get('/register',['as'=>'register-form','uses'=>'Guest\AuthController@registerForm']);
	Route::post('/register/save',['as'=>'register-post','uses'=>'Guest\AuthController@RegisterPost']);
	Route::get('/email/verify',['as'=>'email-verify-page','uses'=>'Guest\EmailVerifyController@emailVerify']);
	Route::get('/email/verify/resend',['as'=>'email-resend','uses'=>'Guest\EmailVerifyController@emailResend']);
	Route::get('/email/verify/account/{token}',['as'=>'email-account-verify','uses'=>'Guest\EmailVerifyController@emailAccount']);
});
Route::get('/logout',['as'=>'logout','uses'=>'Guest\AuthController@logout']);
// ROUTE END LOGIN GUEST //

// ROUTE LOGIN ADMIN //
Route::group(['middleware' => 'admin.has.login'],function(){
	Route::get('/login-admin',['as'=>'login-admin-form','uses'=>'Administrator\AuthController@index']);
	Route::post('/login-admin/post',['as'=>'login-admin-post','uses'=>'Administrator\AuthController@login']);
});
Route::get('/logout-admin',['as'=>'logout-admin','uses'=>'Administrator\AuthController@logout']);
// END ROUTE LOGIN ADMIN //

// ROUTE DATATABLES //
Route::get('/posts/datatables/{type}','DatatablesController@dataPosts');
Route::get('/posts/datatables/trash/{type}','DatatablesController@dataTrashPosts');
Route::get('/categories/datatables','DatatablesController@dataCategories');
Route::get('/users/datatables','DatatablesController@dataUsers');
Route::get('/posts-guest/datatables','DatatablesController@dataPostsGuest');
// END ROUTE DATATABLES //

Route::group(['middleware' => 'admin.middleware'],function(){
	Route::group(['prefix' => 'admin'],function() {
		Route::get('/dashboard',['as'=>'dashboard','uses'=>'Administrator\Admin\DashboardController@main']);
		Route::get('/profile',['as'=>'profile-page','uses'=>'Administrator\Admin\DashboardController@profile']);
		Route::get('/profile/change',['as'=>'profile-page','uses'=>'Administrator\Admin\DashboardController@profileChange']);
		Route::put('/profile/change/save',['as'=>'profile-page','uses'=>'Administrator\Admin\DashboardController@profileSave']);
		
		// ROUTE POSTS //
		Route::get('/posts',['as'=>'posts-page','uses'=>'Administrator\Admin\PostController@index']);
		// Route::get('/posts/create',['as'=>'posts-create','uses'=>'Administrator\Admin\PostController@create']);
		// Route::post('/posts/save',['as'=>'posts-save','uses'=>'Administrator\Admin\PostController@save']);
		Route::get('/posts/edit/{id}',['as'=>'posts-edit','uses'=>'Administrator\Admin\PostController@edit']);
		Route::put('/posts/update/{id}',['as'=>'posts-edit','uses'=>'Administrator\Admin\PostController@update']);
		Route::get('/posts/show/{id}',['as'=>'categories-create','uses'=>'Administrator\Admin\PostController@show']);
		Route::get('/posts/download/{id}',['as'=>'posts-download','uses'=>'Administrator\Admin\PostController@download']);
		Route::get('/posts/download/{id}/data/{type}',['as'=>'posts-download','uses'=>'Administrator\Admin\PostController@downloadFile']);
		Route::delete('/posts/delete/{id}/{type}',['as'=>'posts-edit','uses'=>'Administrator\Admin\PostController@delete']);
		Route::get('/posts/view-trash',['as'=>'posts-trash','uses'=>'Administrator\Admin\PostController@dataTrash']);
		Route::get('/posts/view-trash/delete-all',['as'=>'posts-trash','uses'=>'Administrator\Admin\PostController@trashDelete']);
		// END ROUTE POSTS //

		// ROUTE CATEGORIES //
		Route::get('/categories',['as'=>'categories-page','uses'=>'Administrator\Admin\CategoryController@index']);
		Route::get('/categories/create',['as'=>'categories-create','uses'=>'Administrator\Admin\CategoryController@create']);
		Route::post('/categories/save',['as'=>'categories-save','uses'=>'Administrator\Admin\CategoryController@save']);
		Route::get('/categories/edit/{id}',['as'=>'categories-edit','uses'=>'Administrator\Admin\CategoryController@edit']);
		Route::put('/categories/update/{id}',['as'=>'categories-edit','uses'=>'Administrator\Admin\CategoryController@update']);
		Route::delete('/categories/delete/{id}',['as'=>'categories-edit','uses'=>'Administrator\Admin\CategoryController@delete']);
		// END ROUTE CATEGORIES //

		// ROUTE USERS //
		Route::get('/users',['as'=>'users-page','uses'=>'Administrator\Admin\UsersController@index']);
		Route::get('/users/create',['as'=>'users-create','uses'=>'Administrator\Admin\UsersController@create']);
		Route::post('/users/save',['as'=>'users-save','uses'=>'Administrator\Admin\UsersController@save']);
		Route::get('/users/show/{id}',['as'=>'users-show','uses'=>'Administrator\Admin\UsersController@show']);
		Route::get('/users/edit/{id}',['as'=>'users-edit','uses'=>'Administrator\Admin\UsersController@edit']);
		Route::put('/users/update/{id}',['as'=>'users-edit','uses'=>'Administrator\Admin\UsersController@update']);
		Route::delete('/users/delete/{id}',['as'=>'users-edit','uses'=>'Administrator\Admin\UsersController@delete']);
		// END ROUTE USERS //
	});
	Route::group(['prefix' => 'admin-photo'],function() {
		Route::get('/dashboard',['as'=>'dashboard','uses'=>'Administrator\AdminPhoto\DashboardController@main']);
		Route::get('/profile',['as'=>'profile-page','uses'=>'Administrator\AdminPhoto\DashboardController@profile']);
		Route::get('/profile/change',['as'=>'profile-page','uses'=>'Administrator\AdminPhoto\DashboardController@profileChange']);
		Route::put('/profile/change/save',['as'=>'profile-page','uses'=>'Administrator\AdminPhoto\DashboardController@profileSave']);
		
		// ROUTE POSTS //
		Route::get('/posts',['as'=>'posts-page','uses'=>'Administrator\AdminPhoto\PostController@index']);
		// Route::get('/posts/create',['as'=>'posts-create','uses'=>'Administrator\AdminPhoto\PostController@create']);
		// Route::post('/posts/save',['as'=>'posts-save','uses'=>'Administrator\AdminPhoto\PostController@save']);
		Route::get('/posts/edit/{id}',['as'=>'posts-edit','uses'=>'Administrator\AdminPhoto\PostController@edit']);
		Route::put('/posts/update/{id}',['as'=>'posts-edit','uses'=>'Administrator\AdminPhoto\PostController@update']);
		Route::get('/posts/show/{id}',['as'=>'categories-create','uses'=>'Administrator\AdminPhoto\PostController@show']);
		Route::get('/posts/download/{id}',['as'=>'posts-download','uses'=>'Administrator\AdminPhoto\PostController@download']);
		Route::get('/posts/download/{id}/data/{type}',['as'=>'posts-download','uses'=>'Administrator\AdminPhoto\PostController@downloadFile']);
		Route::delete('/posts/delete/{id}/{type}',['as'=>'posts-edit','uses'=>'Administrator\AdminPhoto\PostController@delete']);
		Route::get('/posts/view-trash',['as'=>'posts-trash','uses'=>'Administrator\AdminPhoto\PostController@dataTrash']);
		Route::get('/posts/view-trash/delete-all',['as'=>'posts-trash','uses'=>'Administrator\AdminPhoto\PostController@trashDelete']);
		// END ROUTE POSTS //

		// ROUTE CATEGORIES //
		Route::get('/categories',['as'=>'categories-page','uses'=>'Administrator\AdminPhoto\CategoryController@index']);
		Route::get('/categories/create',['as'=>'categories-create','uses'=>'Administrator\AdminPhoto\CategoryController@create']);
		Route::post('/categories/save',['as'=>'categories-save','uses'=>'Administrator\AdminPhoto\CategoryController@save']);
		Route::get('/categories/edit/{id}',['as'=>'categories-edit','uses'=>'Administrator\AdminPhoto\CategoryController@edit']);
		Route::put('/categories/update/{id}',['as'=>'categories-edit','uses'=>'Administrator\AdminPhoto\CategoryController@update']);
		Route::delete('/categories/delete/{id}',['as'=>'categories-edit','uses'=>'Administrator\AdminPhoto\CategoryController@delete']);
		// END ROUTE CATEGORIES //

		// ROUTE USERS //
		Route::get('/users',['as'=>'users-page','uses'=>'Administrator\AdminPhoto\UsersController@index']);
		Route::get('/users/create',['as'=>'users-create','uses'=>'Administrator\AdminPhoto\UsersController@create']);
		Route::post('/users/save',['as'=>'users-save','uses'=>'Administrator\AdminPhoto\UsersController@save']);
		Route::get('/users/show/{id}',['as'=>'users-show','uses'=>'Administrator\AdminPhoto\UsersController@show']);
		Route::get('/users/edit/{id}',['as'=>'users-edit','uses'=>'Administrator\AdminPhoto\UsersController@edit']);
		Route::put('/users/update/{id}',['as'=>'users-edit','uses'=>'Administrator\AdminPhoto\UsersController@update']);
		Route::delete('/users/delete/{id}',['as'=>'users-edit','uses'=>'Administrator\AdminPhoto\UsersController@delete']);
		// END ROUTE USERS //
	});
	Route::group(['prefix' => 'admin-video'],function() {
		Route::get('/dashboard',['as'=>'dashboard','uses'=>'Administrator\AdminVideo\DashboardController@main']);
		Route::get('/profile',['as'=>'profile-page','uses'=>'Administrator\AdminVideo\DashboardController@profile']);
		Route::get('/profile/change',['as'=>'profile-page','uses'=>'Administrator\AdminVideo\DashboardController@profileChange']);
		Route::put('/profile/change/save',['as'=>'profile-page','uses'=>'Administrator\AdminVideo\DashboardController@profileSave']);
		
		// ROUTE POSTS //
		Route::get('/posts',['as'=>'posts-page','uses'=>'Administrator\AdminVideo\PostController@index']);
		// Route::get('/posts/create',['as'=>'posts-create','uses'=>'Administrator\AdminVideo\PostController@create']);
		// Route::post('/posts/save',['as'=>'posts-save','uses'=>'Administrator\AdminVideo\PostController@save']);
		Route::get('/posts/edit/{id}',['as'=>'posts-edit','uses'=>'Administrator\AdminVideo\PostController@edit']);
		Route::put('/posts/update/{id}',['as'=>'posts-edit','uses'=>'Administrator\AdminVideo\PostController@update']);
		Route::get('/posts/show/{id}',['as'=>'categories-create','uses'=>'Administrator\AdminVideo\PostController@show']);
		Route::get('/posts/download/{id}',['as'=>'posts-download','uses'=>'Administrator\AdminVideo\PostController@download']);
		Route::get('/posts/download/{id}/data/{type}',['as'=>'posts-download','uses'=>'Administrator\AdminVideo\PostController@downloadFile']);
		Route::delete('/posts/delete/{id}/{type}',['as'=>'posts-edit','uses'=>'Administrator\AdminVideo\PostController@delete']);
		Route::get('/posts/view-trash',['as'=>'posts-trash','uses'=>'Administrator\AdminVideo\PostController@dataTrash']);
		Route::get('/posts/view-trash/delete-all',['as'=>'posts-trash','uses'=>'Administrator\AdminVideo\PostController@trashDelete']);
		// END ROUTE POSTS //

		// ROUTE CATEGORIES //
		Route::get('/categories',['as'=>'categories-page','uses'=>'Administrator\AdminVideo\CategoryController@index']);
		Route::get('/categories/create',['as'=>'categories-create','uses'=>'Administrator\AdminVideo\CategoryController@create']);
		Route::post('/categories/save',['as'=>'categories-save','uses'=>'Administrator\AdminVideo\CategoryController@save']);
		Route::get('/categories/edit/{id}',['as'=>'categories-edit','uses'=>'Administrator\AdminVideo\CategoryController@edit']);
		Route::put('/categories/update/{id}',['as'=>'categories-edit','uses'=>'Administrator\AdminVideo\CategoryController@update']);
		Route::delete('/categories/delete/{id}',['as'=>'categories-edit','uses'=>'Administrator\AdminVideo\CategoryController@delete']);
		// END ROUTE CATEGORIES //

		// ROUTE USERS //
		Route::get('/users',['as'=>'users-page','uses'=>'Administrator\AdminVideo\UsersController@index']);
		Route::get('/users/create',['as'=>'users-create','uses'=>'Administrator\AdminVideo\UsersController@create']);
		Route::post('/users/save',['as'=>'users-save','uses'=>'Administrator\AdminVideo\UsersController@save']);
		Route::get('/users/show/{id}',['as'=>'users-show','uses'=>'Administrator\AdminVideo\UsersController@show']);
		Route::get('/users/edit/{id}',['as'=>'users-edit','uses'=>'Administrator\AdminVideo\UsersController@edit']);
		Route::put('/users/update/{id}',['as'=>'users-edit','uses'=>'Administrator\AdminVideo\UsersController@update']);
		Route::delete('/users/delete/{id}',['as'=>'users-edit','uses'=>'Administrator\AdminVideo\UsersController@delete']);
		// END ROUTE USERS //
	});
});

// ROUTE GUEST //
Route::group(['middleware'=>'guest.middleware'],function(){
	Route::get('/home',['as'=>'guest-page','uses'=>'Guest\HomeController@index']);
	Route::get('/profile',['as'=>'profile-page','uses'=>'Guest\HomeController@profile']);
	Route::get('/profile/edit',['as'=>'profile-form','uses'=>'Guest\HomeController@profileForm']);
	Route::put('/profile/change/save',['as'=>'profile-form','uses'=>'Guest\HomeController@profileSave']);

	// ROUTE POSTS //
	Route::get('/posts',['as'=>'posts-page','uses'=>'Guest\PostController@index']);
	Route::get('/posts/create',['as'=>'posts-form','uses'=>'Guest\PostController@create']);
	Route::get('/posts/edit/{id}',['as'=>'posts-form','uses'=>'Guest\PostController@edit']);
	Route::put('/posts/update/{id}',['as'=>'posts-form','uses'=>'Guest\PostController@update']);
	Route::get('/posts/show/{id}',['as'=>'posts-form','uses'=>'Guest\PostController@show']);
	Route::post('/posts/save',['as'=>'posts-form','uses'=>'Guest\PostController@save']);
	Route::delete('/posts/delete/{id}',['as'=>'posts-form','uses'=>'Guest\PostController@delete']);
	// END ROUTE POSTS //
});
// END ROUTE GUEST //