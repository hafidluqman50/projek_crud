<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class GuestHasLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('guest')->check()) {
            if (Auth::guard('guest')->user()->verified_acc == 0) {
                true;
            }
            else {
                return redirect('/home');
            }
        }
        else if (Auth::guard('admin')->check()) {
            return redirect('/login-admin');
        }
        return $next($request);
    }
}
