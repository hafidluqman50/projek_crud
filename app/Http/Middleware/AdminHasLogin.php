<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminHasLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->check()) {
            if (Auth::guard('admin')->user()->level == 1) {
                return redirect('/admin/dashboard');
            }
            else if (Auth::guard('admin')->user()->level == 2) {
                return redirect('/admin-photo/dashboard');
            }
            else if (Auth::guard('admin')->user()->level == 3) {
                return redirect('/admin-video/dashboard');
            }
        }
        else if (Auth::guard('guest')->check()) {
            return redirect('/');
        }
        return $next($request);
    }
}
