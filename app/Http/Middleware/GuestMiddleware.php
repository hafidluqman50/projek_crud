<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class GuestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('guest')->check()) {
            if (Auth::guard('guest')->user()->verified_acc == 1) {
                true;
            }
            else {
                return redirect('/email/verify');
            }
        }
        else if(Auth::guard('admin')->check()) {
            return redirect('/login-admin');
        }
        else {
            return redirect('/');
        }

        return $next($request);
    }
}
