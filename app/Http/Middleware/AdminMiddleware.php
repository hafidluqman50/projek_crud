<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->check()) {
            if (Auth::guard('admin')->user()->level == 1) {
                true;
            }
            else if (Auth::guard('admin')->user()->level == 2) {
                true;
            }
            else if (Auth::guard('admin')->user()->level == 3) {
                true;
            }
        }
        else if(Auth::guard('guest')->check()) {
            return redirect('/');
        }
        else {
            return redirect('/login-admin');
        }
        return $next($request);
    }
}
