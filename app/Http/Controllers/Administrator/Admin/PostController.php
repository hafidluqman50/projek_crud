<?php

namespace App\Http\Controllers\Administrator\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use Storage;
use Auth;

class PostController extends Controller
{
    public function index()
    {
  		$title = 'Posts';
  		$page  = 'posts';
    	return view('Administrator.Admin.posts.index',compact('title','page'));
    }

    public function edit($id)
    {
        $title      = 'Posts Edit';
        $page       = 'posts';
        $categories = Category::all();
        $row        = Post::where('post_id',$id)->firstOrFail();

        return view('Administrator.Admin.posts.edit',compact('title','page','categories','row','id'));
    }

    public function update(Request $request,$id)
    {
        $title     = $request->title;
        $caption   = $request->caption;
        $file      = $request->file;
        $fileName1 = $file != null ? uniqid('_file_').'_'.$file->getClientOriginalName() : null;
        $file2     = $request->file2;
        $fileName2 = $file2 != null ? uniqid('_file2_').'_'.$file2->getClientOriginalName() : null;
        $audio     = $request->audio;
        $audioName = $audio != null ? uniqid('_audio_').'_'.$audio->getClientOriginalName() : null;
        $width     = $request->width;
        $height    = $request->height;
        $type      = $request->type;
        $category  = $request->category;
        $latitude  = $request->latitude;
        $longitude = $request->longitude;
        $post      = Post::where('post_id',$id);
        $fileOld1  = $post->firstOrFail()->file;
        $fileOld2  = $post->firstOrFail()->file2;
        $fileOld3  = $post->firstOrFail()->audio;

        $data_post = [
            'title'       => $title,
            'caption'     => $caption,
            'file'        => $fileName1,
            'file2'       => $fileName2,
            'audio'       => $audioName,
            'width'       => $width,
            'height'      => $height,
            'type_post'   => $type,
            'catId'       => $category,
            'latitude'    => $latitude,
            'longitude'   => $longitude,
        ];

        if ($file != null && $file2 != null && $audio != null) {
            replace_file($fileOld1,'assets/file/',$fileName1,$file);
            replace_file($fileOld2,'assets/file2/',$fileName2,$file2);
            replace_file($fileOld3,'assets/audio/',$audioName,$audio);
            $post->update($data_post);
        }
        else if($file == null && $file2 != null && $audio != null) {
            unset($data_post['file']);
            replace_file($fileOld2,'assets/file2/',$fileName2,$file2);
            replace_file($fileOld3,'assets/audio/',$audioName,$audio);
            $post->update($data_post);
        }
        else if($file != null && $file2 == null && $audio != null) {
            unset($data_post['file2']);
            replace_file($fileOld1,'assets/file/',$fileName1,$file);
            replace_file($fileOld3,'assets/audio/',$audioName,$audio);
            $post->update($data_post);
        }
        else if($file != null && $file2 != null && $audio == null) {
            unset($data_post['audio']);
            replace_file($fileOld1,'assets/file/',$fileName1,$file);
            replace_file($fileOld2,'assets/file2/',$fileName2,$file2);
            $post->update($data_post);
        }
        else if($file != null && $file2 == null && $audio == null) {
            unset($data_post['file2']);
            unset($data_post['audio']);
            replace_file($fileOld1,'assets/file/',$fileName1,$file);
            $post->update($data_post);  
        }
        else if($file == null && $file2 != null && $audio == null) {
            unset($data_post['file']);
            unset($data_post['audio']);
            replace_file($fileOld2,'assets/file2/',$fileName2,$file2);
            $post->update($data_post);
        }
        else if($file == null && $file2 == null && $audio != null) {
            unset($data_post['file']);
            unset($data_post['file2']);
            replace_file($fileOld3,'assets/audio/',$audioName,$audio);
            $post->update($data_post);
        }
        else {
            unset($data_post['file']);
            unset($data_post['file2']);
            unset($data_post['audio']);
            $post->update($data_post);
        }

        return redirect('/admin/posts')->with('message','Succesful Update Post');
    }

    public function delete($id,$type)
    {
    	if ($type == 'soft-delete') {
            Post::where('post_id',$id)->update(['soft_delete' => 1]);
            $message = 'Data Posts Successful Soft Delete';
    	}

    	else if($type == 'force-delete') {
            Post::where('post_id',$id)->delete();
            $message = 'Data Posts Successful Permanently Delete';
    	}

    	return redirect('/admin/posts')->with('message',$message);
    }

    public function show($id)
    {
        $title = 'Posts Detail';
        $page  = 'posts';
        $row   = Post::detailById($id);

        return view('Administrator.Admin.posts.show',compact('title','page','row'));
    }

    public function dataTrash()
    {
        $title = 'Posts Trash';
        $page  = 'posts';

        return view('Administrator.Admin.posts.view-trash',compact('title','page'));
    }

    public function trashDelete()
    {
        Post::where('soft_delete',1)->delete();

        return redirect('/admin/posts/view-trash')->with('message','Successful Delete All');
    }

    public function download($id)
    {
        $title = 'Posts Download';
        $page  = 'posts';

        return view('Administrator.Admin.posts.download',compact('title','page','id'));
    }

    public function downloadFile($id,$type)
    {
        $post = Post::where('post_id',$id)->firstOrFail();

        // if ($type == 'all') {
        //     $file = public_path('file/'.$post->file);
        //     Storage::download($file);
        //     $file2 = public_path('file2/'.$post->file2);
        //     Storage::download($file2);
        //     $audio = public_path('audio/'.$post->audio);
        //     Storage::download($audio);
        // }
        if ($type == 'file') {
            $download = response()->download(public_path("assets/file/$post->file"));
        }
        else if($type == 'file2') {
            $download = response()->download(public_path("assets/file2/$post->file2"));
        }
        else if($type == 'audio') {
            $download = response()->download(public_path("assets/audio/$post->audio"));   
        }

        return $download;
    }
}
