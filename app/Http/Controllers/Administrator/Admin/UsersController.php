<?php

namespace App\Http\Controllers\Administrator\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    public function index()
    {
		$title = 'Users';
		$page  = 'users';
    	return view('Administrator.Admin.users.index',compact('title','page'));
    }

    public function show($id)
    {
        $title = 'Users Detail';
        $page  = 'users';
        $row   = User::where('user_id',$id)->firstOrFail();
        return view('Administrator.Admin.users.show',compact('title','page','row'));
    }

    public function delete($id)
    {
        User::where('user_id',$id)->delete();

    	return redirect('/admin/users')->with('message','Succesful Delete User');
    }
}
