<?php

namespace App\Http\Controllers\Administrator\AdminVideo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
		$title = 'Categories';
		$page  = 'categories';
    	return view('Administrator.AdminVideo.categories.index',compact('title','page'));
    }

    public function create()
    {
		$title = 'Categories Create';
		$page  = 'categories';
    	return view('Administrator.AdminVideo.categories.create',compact('title','page'));
    }

    public function edit($id)
    {
        $title = 'Categories Edit';
        $page  = 'categories';
        $row   = Category::where('category_id',$id)->firstOrFail();
    	return view('Administrator.AdminVideo.categories.edit',compact('title','page','id','row'));
    }

    public function delete($id)
    {
        Category::where('category_id',$id)->update(['status_delete'=>1]);

    	return redirect('/admin-video/categories')->with('message','Delete Category Successful');
    }

    public function save(Request $request)
    {
        $name     = $request->name;
        $icon     = $request->icon;
        $fileName = uniqid('_icon_cat_').$icon->getClientOriginalName();

        $data_categories = [
            'name' => $name,
            'icon' => $fileName
        ];

        $icon->move(public_path('icon_category'),$fileName);

        Category::create($data_categories);

        return redirect('/admin-video/categories')->with('message','Input Category Successful');
    }

    public function update(Request $request,$id)
    {
        $name     = $request->name;
        $icon     = $request->icon;
        $fileName = $icon != null ? uniqid('_icon_cat_').$icon->getClientOriginalName() : null;
        $category = Category::where('category_id',$id);

        $data_categories = [
            'name' => $name,
            'icon' => $fileName
        ];

        if ($icon != null) {
            $fileOld = $category->icon;
            replace_file($fileOld,'assets/icon_category/',$fileName,$icon);
            $category->update($data_categories);
        }
        else {
            unset($data_categories['icon']);
            $category->update($data_categories);
        }

        return redirect('/admin-video/categories')->with('message','Succesful Edit Category');
    	
    }
}
