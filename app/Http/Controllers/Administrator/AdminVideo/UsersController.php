<?php

namespace App\Http\Controllers\Administrator\AdminVideo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    public function index()
    {
		$title = 'Users';
		$page  = 'users';
    	return view('Administrator.AdminVideo.users.index',compact('title','page'));
    }

    public function show($id)
    {
        $title = 'Users Detail';
        $page  = 'users';
        $row   = User::where('user_id',$id)->firstOrFail();
        return view('Administrator.AdminVideo.users.show',compact('title','page','row'));
    }

    public function delete($id,$type)
    {
    	if ($type == 'soft-delete') {

    	}

    	else if($type == 'force-delete') {

    	}

    	return redirect('/admin-video/users')->with('message',$message);
    }
}
