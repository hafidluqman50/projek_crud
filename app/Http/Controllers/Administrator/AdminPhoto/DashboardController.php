<?php

namespace App\Http\Controllers\Administrator\AdminPhoto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use Auth;

class DashboardController extends Controller
{
    public function main()
    {
		$title = 'Dashboard';
		$page  = 'dashboard';
    	return view('Administrator.AdminPhoto.dashboard',compact('title','page'));
    }

    public function profile()
    {
		$title = 'Profile';
		$page  = 'profile';
		$row   = Auth::guard('admin')->user();
    	return view('Administrator.AdminPhoto.profile',compact('title','page','row'));
    }

    public function profileChange()
    {
		$title = 'Profile Change';
		$page  = 'profile';
		$row   = Auth::guard('admin')->user();
    	return view('Administrator.AdminPhoto.profile-form',compact('title','page','row'));	
    }

    public function profileSave(Request $request)
    {
		$admin 		   = Admin::where('admin_id',Auth::guard('admin')->id());
		$name          = $request->name;
		$username      = $request->username == null ? $admin->firstOrFail()->username : $request->username;
		$check		   = $request->username != null ? Admin::where('username',$username)->exists() : false;
		
		if ($check) {
			return redirect()->back()->withErrors(['username'=>'That username has already taken!'])->withInput($request->all());
		}
		$password      = $request->password;
		$phone_number  = $request->phone_number;
		$photo_profile = $request->photo_profile;
		$fileName      = $photo_profile != null ? uniqid('_admin_photo_profile_').$photo_profile->getClientOriginalName() : null;
		$fileOld	   = $admin->firstOrFail()->foto;

		$data_admin = [
			'name'     => $name,
			'username' => $username,
			'password' => bcrypt($password),
			'no_hp'    => $phone_number,
			'foto'     => $fileName
		];

		if ($password != null && $photo_profile != null) {
			replace_file($fileOld,'assets/admin_photo_profile/',$fileName,$photo_profile);
			$admin->update($data_admin);
		}
		else if ($password != null && $photo_profile == null) {
			unset($data_admin['foto']);
			$admin->update($data_admin);
		}
		else if($password == null && $photo_profile != null) {
			replace_file($fileOld,'assets/admin_photo_profile/',$fileName,$photo_profile);
			unset($data_admin['password']);
			$admin->update($data_admin);
		}
		else {
			unset($data_admin['password']);
			unset($data_admin['foto']);
			$admin->update($data_admin);
		}

		return redirect('/admin-photo/profile')->with('message','Succesful Edit Profile');
    }
}
