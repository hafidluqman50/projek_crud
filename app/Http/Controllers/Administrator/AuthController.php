<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use Auth;

class AuthController extends Controller
{
	public function index()
	{
		return view('Administrator.login');
	}

	public function login(Request $request)
	{
		$admin = $request->only('username','password');
		if (Auth::guard('admin')->attempt($admin)) {
			if (Auth::guard('admin')->user()->level == 1) {
				return redirect()->intended('/admin/dashboard');
			}
			else if(Auth::guard('admin')->user()->level == 2) {
				return redirect()->intended('/admin-photo/dashboard');
			}
			else if(Auth::guard('admin')->user()->level == 3) {
				return redirect()->intended('/admin-video/dashboard');
			}
		}
		else {
			return redirect('/login-admin')->with('log','Credential Not Found');
		}
	}

	public function logout()
	{
		Auth::guard('admin')->logout();

		return redirect('/login-admin');
	}
}
