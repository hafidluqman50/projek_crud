<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Events\EmailSend;
use App\Models\User;
use Auth;

class EmailVerifyController extends Controller
{
    public function emailVerify()
    {
        $title = 'Email Verify';
        $page  = 'email';

    	return view('Guest.email-verify',compact('title','page'));
    }

    public function emailResend()
    {
		$email      = Auth::guard('guest')->user()->email;
		$url_verify = Auth::guard('guest')->user()->url_verify;

		event(new EmailSend(['email' => $email,'url_verify' => $url_verify]));

		return redirect('/email/verify')->with('message','Successful Resend');
    }

    public function emailAccount($token)
    {
        User::where('url_verify',$token)
            ->update([
                'email_verified_at' => date('Y-m-d H:i:s'),
                'verified_acc' => 1
            ]);

        return view('Guest.verify-success');
    }
}
