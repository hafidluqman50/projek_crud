<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Events\EmailSend;
use App\Models\User;
use Auth;

class AuthController extends Controller
{
	public function loginForm()
	{
		return view('Guest.login');
	}

	public function loginPost(Request $request)
	{
		$user = $request->only('username','password');		
		if (Auth::guard('guest')->attempt($user)) {
			if (Auth::guard('guest')->user()->verified_acc == 1) {
				return redirect()->intended('/home');
			}
			else {
				$auth      = Auth::guard('guest')->user();
				$data_user = ['email' => $auth->email, 'url_verify' => $auth->url_verify];
				
				event(new EmailSend($data_user));
				return redirect()->intended('/email/verify');
			}
		}
		else {
			return redirect('/')->with('log','User Not Found');
		}
	}

	public function registerForm()
	{
		return view('Guest.register');
	}

	public function registerPost(Request $request)
	{
		$nick          = $request->nick;
		$username      = $request->username;
		$check		   = User::where('username',$username)->exists();
		if ($check) {
			return redirect()->back()->withErrors(['username'=>'That username has already taken!'])->withInput($request->all());
		}

		$password      = bcrypt($request->password);
		$email         = $request->email;
		$gender        = $request->gender;
		$bio           = $request->bio;
		$birthday      = $request->birthday;
		$photo_profile = $request->photo_profile;
		$fileName      = uniqid('_photo_profile_').'_'.$photo_profile->getClientOriginalName();
		$photo_profile->move(public_path('assets/photo_profile'),$fileName);

		$data_user = [
			'nick'          => $nick,
			'username'      => $username,
			'password'      => $password,
			'email'         => $email,
			'gender'        => $gender,
			'bio'           => $bio,
			'tanggal_lahir' => $birthday,
			'umur'			=> count_age($birthday),
			'photo_profile' => $fileName,
			'url_verify'	=> sha1(time())
		];

		$user_id = User::insertGetId($data_user);

		event(new EmailSend(['email' => $data_user['email'],'url_verify' => $data_user['url_verify']]));

		Auth::guard('guest')->loginUsingId($user_id);

		return redirect('/email/verify');
	}

	public function logout()
	{
		Auth::guard('guest')->logout();

		return redirect('/');
	}
}
