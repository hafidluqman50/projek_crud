<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use Auth;

class PostController extends Controller
{
    public function index()
    {
		$title = 'Posts';
		$page  = 'posts';

		return view('Guest.posts.index',compact('title','page'));
    }

    public function create()
    {
		$title      = 'Posts Create';
		$page       = 'posts';
		$categories = Category::all();

		return view('Guest.posts.create',compact('title','page','categories'));
    }

    public function edit($id)
    {
		$title      = 'Posts Edit';
		$page       = 'posts';
		$categories = Category::all();
		$row = Post::where('post_id',$id)->firstOrFail();

		return view('Guest.posts.edit',compact('title','page','categories','row','id'));
    }

    public function show($id)
    {
		$title = 'Posts Detail';
		$page  = 'posts';
		$row   = Post::detailById($id);

		return view('Guest.posts.show',compact('title','page','row'));
    }

    public function delete($id)
    {
    	Post::where('post_id',$id)->update(['soft_delete'=>1]);

    	return redirect('/posts')->with('message','Successful Delete');
    }

    public function save(Request $request)
    {
		$title     = $request->title;
		$caption   = $request->caption;
		$file      = $request->file;
		$fileName1 = uniqid('_file_').'_'.$file->getClientOriginalName();
		$file2     = $request->file2;
		$fileName2 = uniqid('_file2_').'_'.$file2->getClientOriginalName();
		$audio	   = $request->audio;
		$audioName = uniqid('_audio_').'_'.$audio->getClientOriginalName();
		$width	   = $request->width;
		$height	   = $request->height;
		$type	   = $request->type;
		$category  = $request->category;
		$latitude  = $request->latitude;
		$longitude = $request->longitude;

		$data_post = [
			'title'       => $title,
			'caption'     => $caption,
			'file'        => $fileName1,
			'file2'       => $fileName2,
			'audio'       => $audioName,
			'width'       => $width,
			'height'      => $height,
			'type_post'   => $type,
			'userId'      => Auth::guard('guest')->id(),
			'catId'       => $category,
			'latitude'    => $latitude,
			'longitude'   => $longitude,
			'soft_delete' => 0,
		];

		$file->move(public_path('assets/file'),$fileName1);
		$file2->move(public_path('assets/file2'),$fileName2);
		$audio->move(public_path('assets/audio'),$audioName);

		Post::create($data_post);

		return redirect('/posts')->with('message','Successful Input Post');
    }

    public function update(Request $request,$id)
    {
		$title     = $request->title;
		$caption   = $request->caption;
		$file      = $request->file;
		$fileName1 = $file != null ? uniqid('_file_').'_'.$file->getClientOriginalName() : null;
		$file2     = $request->file2;
		$fileName2 = $file2 != null ? uniqid('_file2_').'_'.$file2->getClientOriginalName() : null;
		$audio	   = $request->audio;
		$audioName = $audio != null ? uniqid('_audio_').'_'.$audio->getClientOriginalName() : null;
		$width	   = $request->width;
		$height	   = $request->height;
		$type	   = $request->type;
		$category  = $request->category;
		$latitude  = $request->latitude;
		$longitude = $request->longitude;
		$post 	   = Post::where('post_id',$id);
		$fileOld1  = $post->firstOrFail()->file;
		$fileOld2  = $post->firstOrFail()->file2;
		$fileOld3  = $post->firstOrFail()->audio;

		$data_post = [
			'title'       => $title,
			'caption'     => $caption,
			'file'        => $fileName1,
			'file2'       => $fileName2,
			'audio'       => $audioName,
			'width'       => $width,
			'height'      => $height,
			'type_post'   => $type,
			'catId'       => $category,
			'latitude'    => $latitude,
			'longitude'   => $longitude,
		];

		if ($file != null && $file2 != null && $audio != null) {
			replace_file($fileOld1,'assets/file/',$fileName1,$file);
			replace_file($fileOld2,'assets/file2/',$fileName2,$file2);
			replace_file($fileOld3,'assets/audio/',$audioName,$audio);
			$post->update($data_post);
		}
		else if($file == null && $file2 != null && $audio != null) {
			unset($data_post['file']);
			replace_file($fileOld2,'assets/file2/',$fileName2,$file2);
			replace_file($fileOld3,'assets/audio/',$audioName,$audio);
			$post->update($data_post);
		}
		else if($file != null && $file2 == null && $audio != null) {
			unset($data_post['file2']);
			replace_file($fileOld1,'assets/file/',$fileName1,$file);
			replace_file($fileOld3,'assets/audio/',$audioName,$audio);
			$post->update($data_post);
		}
		else if($file != null && $file2 != null && $audio == null) {
			unset($data_post['audio']);
			replace_file($fileOld1,'assets/file/',$fileName1,$file);
			replace_file($fileOld2,'assets/file2/',$fileName2,$file2);
			$post->update($data_post);
		}
		else if($file != null && $file2 == null && $audio == null) {
			unset($data_post['file2']);
			unset($data_post['audio']);
			replace_file($fileOld1,'assets/file/',$fileName1,$file);
			$post->update($data_post);	
		}
		else if($file == null && $file2 != null && $audio == null) {
			unset($data_post['file']);
			unset($data_post['audio']);
			replace_file($fileOld2,'assets/file2/',$fileName2,$file2);
			$post->update($data_post);
		}
		else if($file == null && $file2 == null && $audio != null) {
			unset($data_post['file']);
			unset($data_post['file2']);
			replace_file($fileOld3,'assets/audio/',$audioName,$audio);
			$post->update($data_post);
		}
		else {
			unset($data_post['file']);
			unset($data_post['file2']);
			unset($data_post['audio']);
			$post->update($data_post);
		}

		return redirect('/posts')->with('message','Succesful Update Post');
    }
}
