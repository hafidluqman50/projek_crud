<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
    public function index()
    {
		$title = 'Home';
		$page  = 'home';

    	return view('Guest.dashboard',compact('title','page'));
    }

    public function profile()
    {
		$title = 'Profile';
		$page  = 'profile';
		$row   = User::where('user_id',Auth::guard('guest')->id())->firstOrFail();

		return view('Guest.profile',compact('title','page','row'));
    }

    public function profileForm()
    {
		$title = 'Profile Edit';
		$page  = 'profile';
		$row   = User::where('user_id',Auth::guard('guest')->id())->firstOrFail();

		return view('Guest.profile-form',compact('title','page','row'));
    }

    public function profileSave(Request $request)
    {
		$user          = User::where('user_id',Auth::guard('guest')->id());
		$nick          = $request->nick;
		$username      = $request->username == null ? $user->firstOrFail()->username : $request->username;
		$check		   = $request->username != null ? User::where('username',$username)->exists() : false;
		
		if ($check) {
			return redirect()->back()
							 ->withErrors(['username'=>'That username has already taken!'])
							 ->withInput($request->all());
		}

		$password      = $request->password;
		$email         = $request->email;
		$gender        = $request->gender;
		$birthday      = $request->birthday;
		$bio           = $request->bio;
		$birthday      = $request->birthday;
		$photo_profile = $request->photo_profile;
		$fileName      = $photo_profile != null ? uniqid('_photo_profile_').'_'.$photo_profile->getClientOriginalName() : null;
		$fileOld	   = $user->firstOrFail()->photo_profile;

		$data_user = [
			'nick'          => $nick,
			'username'      => $username,
			'password'      => bcrypt($password),
			'email'         => $email,
			'gender'        => $gender,
			'bio'           => $bio,
			'tanggal_lahir' => $birthday,
			'umur'			=> count_age($birthday),
			'photo_profile'	=> $fileName
		];

		if ($password != null && $photo_profile != null) {
			replace_file($fileOld,'assets/photo_profile/',$fileName,$photo_profile);
			$user->update($data_user);
		}
		else if ($password != null && $photo_profile == null) {
			unset($data_user['photo_profile']);
			$user->update($data_user);
		}
		else if($password == null && $photo_profile != null) {
			replace_file($fileOld,'assets/photo_profile/',$fileName,$photo_profile);
			unset($data_user['password']);
			$user->update($data_user);
		}
		else {
			unset($data_user['password']);
			unset($data_user['photo_profile']);
			$user->update($data_user);
		}

		return redirect('/profile')->with('message','Succesful Edit Profile');
    }
}
