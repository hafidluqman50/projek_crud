<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\User;
use Auth;
use DataTables;

class DatatablesController extends Controller
{
    private $level;

    public function __construct()
    {
        $this->middleware(function($request,$next){
            if (Auth::guard('admin')->check()) {
                $this->level = Auth::guard('admin')->user()->level == 1 ? 'admin' : (Auth::guard('admin')->user()->level == 2 ? 'admin-photo' : (Auth::guard('admin')->user()->level == 3 ? 'admin-video' : ''));

            }
            return $next($request);
        });
    }

    public function dataPosts($type)
    {
    	$posts = Post::getData($type);
    	$datatables = DataTables::of($posts)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/posts/show/$action->post_id").'">
					   		<button class="btn btn-info"> Detail </button>
					    </a>
                        <a href="'.url("/$this->level/posts/edit/$action->post_id").'">
                            <button class="btn btn-warning"> Edit </button>
                        </a>
                        <a href="'.url("/$this->level/posts/download/$action->post_id").'">
                            <button class="btn btn-success"> Download </button>
                        </a>
					    <form action="'.url("/$this->level/posts/delete/$action->post_id/soft-delete").'" method="POST">
					    	<input type="hidden" name="_token" value="'.csrf_token().'">
					    	<input type="hidden" name="_method" value="DELETE">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Delete ?\');"> Delete </button>
					   	 </form>';

            if ($this->level == 'admin') {
                $column .= '<form action="'.url("/$this->level/posts/delete/$action->post_id/force-delete").'" method="POST">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                            <input type="hidden" name="_method" value="DELETE">
                           <button class="btn btn-danger" onclick="return confirm(\'Force Hapus ?\');"> Force Delete </button>
                         </form>';
            }
    		return $column;
    	})->editColumn('type_post',function($edit){
            return ucwords($edit->type_post);
        })->make(true);

    	return $datatables;
    }

    public function dataTrashPosts($type)
    {
        $trash_posts = Post::trashData($type);
        $datatables = DataTables::of($trash_posts)->addColumn('action',function($action){
            $column = '<form action="'.url("/$this->level/posts/delete/$action->post_id/force-delete").'" method="POST">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                            <input type="hidden" name="_method" value="DELETE">
                           <button class="btn btn-danger" onclick="return confirm(\'Delete ?\');"> Delete </button>
                         </form>
                    ';
            return $column;
        })->make(true);

        return $datatables;

    }

    public function dataCategories()
    {
    	$categories = Category::where('status_delete',0)->get();
    	$datatables = DataTables::of($categories)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/categories/edit/$action->category_id").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					    <form action="'.url("/$this->level/categories/delete/$action->category_id").'" method="POST">
					    	<input type="hidden" name="_token" value="'.csrf_token().'">
					    	<input type="hidden" name="_method" value="DELETE">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Delete ?\');"> Delete </button>
					   	 </form>
    				';
    		return $column;
    	})->editColumn('icon',function($edit){
            return '<img src="'.asset("/assets/icon_category/$edit->icon").'" style="width:20%; height:20%" draggable="false">';
        })->rawColumns(['action','icon'])->make(true);

    	return $datatables;
    }

    public function dataUsers()
    {
        $users = User::all();
        $datatables = DataTables::of($users)->addColumn('action',function($action){
            $column = '<form action="'.url("/$this->level/users/delete/$action->user_id").'" method="POST">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                            <input type="hidden" name="_method" value="DELETE">
                           <button class="btn btn-danger" onclick="return confirm(\'Delete ?\');"> Delete </button>
                         </form>';
            // $column .= '<a href="'.url("/$this->level/users/show/$action->user_id").'">
            //                 <button class="btn btn-info"> Detail </button>
            //             </a>';
            return $column;
        })->make(true);

        return $datatables;
    }

    public function dataPostsGuest()
    {
        $user_id = Auth::guard('guest')->id();
        $posts   = Post::where('userId',$user_id)->where('soft_delete',0)->get();
        $datatables = DataTables::of($posts)->addColumn('action',function($action){
            $column = '<a href="'.url("/posts/edit/$action->post_id").'">
                          <button class="btn btn-warning"> Edit </button>
                       </a>
                        <a href="'.url("/posts/show/$action->post_id").'">
                            <button class="btn btn-info"> Detail </button>
                        </a>
                        <form action="'.url("/posts/delete/$action->post_id").'" method="POST">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                            <input type="hidden" name="_method" value="DELETE">
                           <button class="btn btn-danger" onclick="return confirm(\'Delete ?\');"> Delete </button>
                         </form>
                    ';
            return $column;
        })->editColumn('type_post',function($edit){
            return ucwords($edit->type_post);
        })->make(true);

        return $datatables;
    }
}
