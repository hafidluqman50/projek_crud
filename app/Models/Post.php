<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $table      = 'posts';
	protected $primaryKey = 'post_id';
	protected $guarded 	  = [];

	public static function getData($type)
	{
		if ($type == 'all') 
		{
			$db = self::join('users','posts.userId','=','users.user_id')
					  ->where('soft_delete',0)
					  ->get();
		}
		else if($type == 'video' || $type == 'photo')
		{
			$db = self::join('users','posts.userId','=','users.user_id')
					  ->where('type_post',$type)
					  ->where('soft_delete',0)
					  ->get();
		}

		return $db;
	}

	public static function detailById($id)
	{
		$db = self::join('users','posts.userId','=','users.user_id')
				  ->join('categories','posts.catId','=','categories.category_id')
				  ->where('post_id',$id)
				  ->firstOrFail();

		return $db;
	}

	public static function trashData($type)
	{
		if ($type == 'all') 
		{
			$db = self::join('users','posts.userId','=','users.user_id')
					  ->where('soft_delete',1)
					  ->get();
		}
		else if($type == 'video' || $type == 'photo')
		{
			$db = self::join('users','posts.userId','=','users.user_id')
					  ->where('type',$type)
					  ->where('soft_delete',1)
					  ->get();
		}

		return $db;
	}
}
