<?php

namespace App\Listeneres;

use App\Events\EmailSend;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyAccount;

class SendEmailVerify
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  EmailSend  $event
     * @return void
     */
    public function handle(EmailSend $event)
    {
        $token      = $event->user->url_verify;
        $url_verify = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/email/verify/account/$token";

        Mail::to($event->user->email)->send(new VerifyAccount($url_verify));
    }
}
