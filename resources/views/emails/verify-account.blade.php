@component('mail::message')
# Verify Account

Hey thanks for register an account, click here for verify your account now

@component('mail::button', ['url' => $url_verify])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
