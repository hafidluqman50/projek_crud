@extends('Guest.layout.layout-app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('/profile') }}">
							<button class="btn btn-dark">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
					</div>
					<form action="{{ url('/profile/change/save') }}" method="POST" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label for="" class="col-md-4 col-form-label text-md-right">
											Nick
										</label>
										<div class="col-md-6">
											<input type="text" name="nick" class="form-control" value="{{ old('nick') != null ? old('nick') : $row->nick }}" required="required">
										</div>
									</div>
									<div class="form-group row">
										<label for="" class="col-md-4 col-form-label text-md-right">
											Email
										</label>
										<div class="col-md-6">
											<input type="email" name="email" class="form-control" value="{{ old('email') != null ? old('email') : $row->email }}" required="required">
										</div>
									</div>
									<div class="form-group row">
										<label for="" class="col-md-4 col-form-label text-md-right">
											Username
										</label>
										<div class="col-md-6">
											<input type="text" name="username" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') != null ? old('username') : $row->username }}" required="required" disabled>
											@error('username')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
											@enderror
											<input type="checkbox" id="check"> Change Username
										</div>
									</div>
									<div class="form-group row">
										<label for="" class="col-md-4 col-form-label text-md-right">
											Password
										</label>
										<div class="col-md-6">
											<input type="password" name="password" class="form-control">
										</div>
									</div>
									<div class="form-group row">
										<label for="" class="col-md-4 col-form-label text-md-right">
											Gender
										</label>
										<div class="col-md-6">
											<select name="gender" class="form-control" required="required">
												<option value="" selected disabled>=== Select Gender ===</option>
												<option value="male" {!!$row->gender == 'male' || old('gender') == 'male' ? 'selected="selected"' : ''!!}>Male</option>
												<option value="female" {!!$row->gender == 'female' || old('gender') == 'female' ? 'selected="selected"' : ''!!}>Female</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<label for="" class="col-md-4 col-form-label text-md-right">
											Birthday
										</label>
										<div class="col-md-6">
											<input type="date" name="birthday" class="form-control" value="{{ old('birthday') != null ? old('birthday') : $row->tanggal_lahir }}" required="required">
										</div>
									</div>
									<div class="form-group row">
										<label for="" class="col-md-4 col-form-label text-md-right">
											Bio
										</label>
										<div class="col-md-6">
											<textarea name="bio" class="form-control" cols="30" rows="5">{{old('bio') != null ? old('bio') : $row->bio}}</textarea>
										</div>
									</div>
									<div class="form-group row">
										<label for="" class="col-md-4 col-form-label text-md-right">
											Photo Profile
										</label>
										<div class="col-md-6">
											<input type="file" name="photo_profile" class="form-control" id="image">
										</div>
										<img src="" class="img-fluid" id="uploadPreview" alt="">
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<div class="form-group row mb-0">
								<div class="col-md-6 offset-md-1">
									<button class="btn btn-primary">
										Save
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection