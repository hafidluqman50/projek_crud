<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login Form</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
	<main class="py-4">
		<div class="container">
		    <div class="row justify-content-center">
		        <div class="col-md-8">
		            <div class="card">
		                <div class="card-header">{{ __('Login') }}</div>

		                <form action="{{ url('login') }}" method="POST">
			                <div class="card-body">
			                        @csrf
								    @if(session()->has('log'))
									<div class="alert alert-danger alert-dismissible">
										{{ session('log') }} <button type="button" class="close" data-dismiss="alert">X</button>
									</div>
								    @endif

			                        <div class="form-group row">
			                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

			                            <div class="col-md-6">
			                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

			                                @error('username')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>

			                        <div class="form-group row">
			                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

			                            <div class="col-md-6">
			                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

			                                @error('password')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
			                            </div>
			                        </div>
			                </div>
			                <div class="card-footer">
		                        <div class="form-group row mb-0">
		                            <div class="col-md-6 offset-md-2">
		                                <button type="submit" class="btn btn-primary">
		                                    {{ __('Login') }}
		                                </button>
		                            </div>
		                            <div class="col-md-4 mt-1">
		                            	Doesn't have account ? <a href="{{ url('/register') }}">Register</a>
		                            </div>
		                        </div>
			                </div>
		                </form>
		            </div>
		        </div>
		    </div>
		</div>
	</main>
</body>
</html>
<!-- jQuery 3 -->
<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('assets/dist/js/custom.js')}}"></script>