@extends('Guest.layout.layout-app')

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">
						Verify Your Email Address
					</div>
					<div class="card-body">
						Before proceedding, please check your email for a verification link, If you didn't receive the email, <a href="{{ url('/email/verify/resend') }}">click here to request again</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection