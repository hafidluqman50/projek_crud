<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Register Form</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
	<main class="py-4">
		<div class="container">
		    <div class="row justify-content-center">
		        <div class="col-md-8">
		            <div class="card">
		                <div class="card-header">{{ __('Register') }}</div>

		                <form method="POST" action="{{ url('/register/save') }}" enctype="multipart/form-data">
			                <div class="card-body">
		                        @csrf

		                        <div class="form-group row">
		                            <label for="nick" class="col-md-4 col-form-label text-md-right">{{ __('Nick') }}</label>

		                            <div class="col-md-6">
		                                <input id="nick" type="text" class="form-control @error('nick') is-invalid @enderror" name="nick" value="{{ old('nick') }}" required autocomplete="nick" autofocus>

		                                @error('nick')
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

		                            <div class="col-md-6">
		                                <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username">

		                                @error('username')
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

		                            <div class="col-md-6">
		                                <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">

		                                @error('password')
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

		                            <div class="col-md-6">
		                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email">

		                                @error('email')
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                        	<label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

		                        	<div class="col-md-6">
		                        		<select name="gender" id="gender" class="form-control" required>
		                        			<option value="" selected disabled>=== Select Gender ===</option>
		                        			<option value="male" {{old('gender') == 'male' ? 'selected' : ''}}>Male</option>
		                        			<option value="female" {{old('gender') == 'male' ? 'selected' : ''}}>Female</option>
		                        		</select>
		                        	</div>
		                        </div>

		                        <div class="form-group row">
		                        	<label for="bio" class="col-md-4 col-form-label text-md-right">{{ __('Bio') }}</label>

		                        	<div class="col-md-6">
		                        		<textarea name="bio" id="bio" class="form-control" cols="30" rows="5">{{old('bio')}}</textarea>
		                        	</div>
		                        </div>

		                        <div class="form-group row">
		                        	<label for="birthday" class="col-md-4 col-form-label text-md-right">{{ __('Birthday') }}</label>

		                        	<div class="col-md-6">
		                        		<input type="date" name="birthday" class="form-control" id="birthday" value="{{ old('birthday') }}">
		                        	</div>
		                        </div>

		                        <div class="form-group row">
		                        	<label for="photo_profile" class="col-md-4 col-form-label text-md-right">{{ __('Photo Profile') }}</label>

		                        	<div class="col-md-6">
		                        		<input type="file" name="photo_profile" class="form-control" id="photo_profile" value="{{old('photo_profile')}}">
		                        	</div>
		                        </div>
			                </div>
			                <div class="card-footer">
		                        <div class="form-group row mb-0">
		                            <div class="col-md-6 offset-md-2">
		                                <button type="submit" class="btn btn-primary">
		                                    {{ __('Register') }}
		                                </button>
		                            </div>
		                            <div class="col-md-4 mt-1">
		                            	Have an account ? <a href="{{ url('/') }}">Login</a>
		                            </div>
		                        </div>
			                </div>
		                </form>
		            </div>
		        </div>
		    </div>
		</div>
	</main>
</body>
</html>
<!-- jQuery 3 -->
<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('assets/dist/js/custom.js')}}"></script>