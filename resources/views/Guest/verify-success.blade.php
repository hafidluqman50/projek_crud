<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Verify Email</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
	<main class="py-4">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							Successful Verify
						</div>
						<div class="card-body">
							Your account succesful verify <a href="{{ url('/') }}">login now</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</body>
</html>