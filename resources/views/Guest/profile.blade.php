@extends('Guest.layout.layout-app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<img class="img-thumbnail rounded mx-auto d-block" src="{{ asset('assets/photo_profile/'.$row->photo_profile) }}" width="20%" height="20%">
					</div>
					<div class="card-body">
					    @if(session()->has('message'))
						<div class="alert alert-success alert-dismissible">
							{{ session('message') }} <button class="close" data-dismiss="alert">X</button>
						</div>
					    @endif
						<div class="row">
							<div class="col-md-6">
								<div class="form-group row">
									<label for="" class="col-md-4 col-form-label text-md-right">Nick</label>
									<div class="col-md-6">
										<input type="text" class="form-control" value="{{ $row->nick }}" readonly>
									</div>
								</div>
								<div class="form-group row">
									<label for="" class="col-md-4 col-form-label text-md-right">Username</label>
									<div class="col-md-6">
										<input type="text" class="form-control" value="{{ $row->username }}" readonly>
									</div>
								</div>
								<div class="form-group row">
									<label for="" class="col-md-4 col-form-label text-md-right">Email</label>
									<div class="col-md-6">
										<input type="text" class="form-control" value="{{ $row->email }}" readonly>
									</div>
								</div>
								<div class="form-group row">
									<label for="" class="col-md-4 col-form-label text-md-right">Gender</label>
									<div class="col-md-6">
										<input type="text" class="form-control" value="{{ ucwords($row->gender) }}" readonly>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group row">
									<label for="" class="col-md-4 col-form-label text-md-right">Bio</label>
									<div class="col-md-6">
										<textarea class="form-control" id="" cols="30" rows="5" readonly>{{ $row->bio }}</textarea>
									</div>
								</div>
								<div class="form-group row">
									<label for="" class="col-md-4 col-form-label text-md-right">Birthday</label>
									<div class="col-md-6">
										<input type="text" class="form-control" value="{{ date_explode($row->tanggal_lahir) }}" readonly>
									</div>
								</div>
								<div class="form-group row">
									<label for="" class="col-md-4 col-form-label text-md-right">Age</label>
									<div class="col-md-6">
										<input type="text" class="form-control" value="{{ $row->umur }}" readonly>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="form-group row mb-0">
							<div class="col-md-6 offset-md-1">
								<a href="{{ url('/profile/edit') }}">
									<button class="btn btn-primary">		
										Edit Profile
									</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection