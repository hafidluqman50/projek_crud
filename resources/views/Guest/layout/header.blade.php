<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $title }}</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{asset('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/datatables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css')}}">
</head>
<body>
{{-- <div id="app"> --}}
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{-- {{ config('app.name', 'Laravel') }} --}}
                Project
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                @if (Auth::guard('guest')->check())
                    @if (Auth::guard('guest')->user()->verified_acc == 1)
                    <ul class="navbar-nav mr-auto">
    					<li class="nav-item {{$page == 'home' ? 'active' : ''}}">
    						<a href="{{ url('/home') }}" class="nav-link">
    							Home
    						</a>
    					</li>
    					<li class="nav-item {{$page == 'posts' ? 'active' : ''}}">
    						<a href="{{ url('/posts') }}" class="nav-link">
    							Posts
    						</a>
    					</li>
                    </ul>
                    @endif
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::guard('guest')->user()->nick }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ url('profile') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('profile-page').submit();">
                                {{ __('Profile') }}
                            </a>

                            <form id="profile-page" action="{{ url('profile') }}" method="GET" style="display: none;">
                                @csrf
                            </form>

                            <a class="dropdown-item" href="{{ url('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ url('logout') }}" method="GET" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
    
{{-- </div> --}}