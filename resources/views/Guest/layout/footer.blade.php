	</main>
</body>
</html>
<!-- jQuery 3 -->
<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/datatables/DataTables-1.10.20/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/datatables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('assets/dist/js/custom.js')}}"></script>