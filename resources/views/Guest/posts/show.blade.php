@extends('Guest.layout.layout-app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('/posts') }}">
							<button class="btn btn-dark">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									@if ($row->type_post == 'photo')
									<img src="{{ asset('assets/file2/'.$row->file2) }}" class="img-fluid" alt="">
									@elseif ($row->type_post == 'video')
									<div class="embed-responsive embed-responsive-16by9">
										<video class="embed-responsive-item" controls>
											<source src="{{ asset('assets/file2/'.$row->file2) }}">
										</video>
									</div>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">Title</label>
									<input type="text" class="form-control" value="{{ $row->title }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Caption</label>
									<input type="text" class="form-control" value="{{ $row->caption }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Category</label>
									<input type="text" class="form-control" value="{{ $row->name }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Width</label>
									<input type="text" class="form-control" value="{{ $row->width }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Height</label>
									<input type="text" class="form-control" value="{{ $row->height }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Type</label>
									<input type="text" class="form-control" value="{{ ucwords($row->type_post) }}" disabled>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">Like</label>
									<input type="text" class="form-control" value="{{ $row->like }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Views</label>
									<input type="text" class="form-control" value="{{ $row->views }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Comment</label>
									<input type="text" class="form-control" value="{{ $row->comment }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Report</label>
									<input type="text" class="form-control" value="{{ $row->report }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Longitude</label>
									<input type="text" class="form-control" value="{{ $row->longitude }}" disabled>
								</div>
								<div class="form-group">
									<label for="">Latitude</label>
									<input type="text" class="form-control" value="{{ $row->latitude }}" disabled>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection