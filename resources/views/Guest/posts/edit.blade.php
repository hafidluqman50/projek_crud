@extends('Guest.layout.layout-app')

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('/posts') }}">
							<button class="btn btn-dark">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
					</div>
					<form action="{{ url('/posts/update/'.$id) }}" method="POST" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="card-body">
							<div class="form-group">
								<label for="">Title</label>
								<input type="text" name="title" class="form-control" required="required" placeholder="Fill Title Input" value="{{ $row->title }}">
							</div>
							<div class="form-group">
								<label for="">Caption</label>
								<textarea name="caption" id="" class="form-control" cols="30" rows="10" required="required" placeholder="Fill Caption Input">{{$row->caption}}</textarea>
							</div>
							<div class="form-group">
								<label for="">File</label>
								<input type="file" name="file" class="form-control">
							</div>
							<div class="form-group">
								<label for="">File 2</label>
								<input type="file" name="file2" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Audio</label>
								<input type="file" name="audio" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Width</label>
								<input type="number" name="width" class="form-control" required="required" placeholder="Fill Width Input" value="{{ $row->width }}">
							</div>
							<div class="form-group">
								<label for="">Height</label>
								<input type="number" name="height" class="form-control" required="required" placeholder="Fill Height Input" value="{{ $row->height }}">
							</div>
							<div class="form-group">
								<label for="">Type</label>
								<select name="type" class="form-control" required="required">
									<option value="" selected disabled>=== Select Type ===</option>
									<option value="photo" {!!$row->type_post == 'photo' ? 'selected="selected"':''!!}>Photo</option>
									<option value="video" {!!$row->type_post == 'video' ? 'selected="selected"':''!!}>Video</option>
								</select>
							</div>
							{{-- <div class="form-group">
								<label for="">Like</label>
								<input type="text" name="like" class="form-control" required="required" placeholder="Fill Like Input">
							</div>
							<div class="form-group">
								<label for="">Views</label>
								<input type="text" name="views" class="form-control" required="required" placeholder="Fill Views Input">
							</div>
							<div class="form-group">
								<label for="">Comment</label>
								<input type="text" name="Comment" class="form-control" required="required" placeholder="Fill Comment Input">
							</div>
							<div class="form-group">
								<label for="">Report</label>
								<input type="text" name="report" class="form-control" required="required" placeholder="Fill Report Input">
							</div> --}}
							<div class="form-group">
								<label for="">Category</label>
								<select name="category" class="form-control select2" required="required">
									<option value="" selected="selected" disabled="disabled">=== Choose Category ===</option>
									@foreach ($categories as $element)
									<option value="{{ $element->category_id }}" {!!$row->catId == $element->category_id ? 'selected="selected"' : ''!!}>{{ $element->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Latitude</label>
								<input type="text" name="latitude" class="form-control" required="required" placeholder="Fill Latitude Input" value="{{ $row->latitude }}">
							</div>
							<div class="form-group">
								<label for="">Longitude</label>
								<input type="text" name="longitude" class="form-control" required="required" placeholder="Fill Longitude Input" value="{{ $row->longitude }}">
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary">
								Save
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection