@extends('Guest.layout.layout-app')

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<h4>Posts</h4>
				<div class="card">
					<div class="card-header">
						<a href="{{ url('/posts/create') }}">
							<button class="btn btn-primary">
								Create
							</button>
						</a>
					</div>
					<div class="card-body">
					    @if(session()->has('message'))
						<div class="alert alert-success alert-dismissible">
							{{ session('message') }} <button class="close" data-dismiss="alert">X</button>
						</div>
					    @endif
						<table class="table table-hover" id="posts-guest" width="100%">
							<thead>
								<tr>
									<th>No.</th>
									<th>Title</th>
									<th>Caption</th>
									<th>Type</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection