@extends('Administrator.Admin.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Categories Create</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="{{ url('/admin/categories') }}">
							<button class="btn btn-default">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
					</div>
					<form action="{{ url('/admin/categories/save') }}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="box-body">
							<div class="form-group">
								<label for="">Name</label>
								<input type="text" name="name" class="form-control" required="required" placeholder="Fill Name Categories Input">
							</div>
							<div class="form-group">
								<label for="">Icon</label>
								<input type="file" name="icon" class="form-control" required="required">
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-primary">
								Save
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection