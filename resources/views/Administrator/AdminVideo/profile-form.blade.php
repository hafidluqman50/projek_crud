@extends('Administrator.Admin.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Profile Change</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="{{ url('/admin-video/profile') }}">
							<button class="btn btn-default">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
					</div>
					<form action="{{ url('/admin-video/profile/change/save') }}" method="POST" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="box-body">
							<div class="form-group">
								<label for="">Name</label>
								<input type="text" name="name" class="form-control" required="required" placeholder="Fill Input Name" value="{{  old('name') != null ? old('name') : $row->name }}">
							</div>
							<div class="form-group">
								<label for="">Username</label>
								<input type="text" name="username" class="form-control" required="required" placeholder="Fill Input Username" value="{{ old('username') != null ? old('username') : $row->username}}" disabled>
								@error('username')
								<div class="alert alert-danger alert-dismissible">
									{{ $message }} <button class="close" data-dismiss="alert">X</button>
								</div>
								@enderror
								<input type="checkbox" id="check"> Change Username
							</div>
							<div class="form-group">
								<label for="">Password</label>
								<input type="password" name="password" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Phone Number</label>
								<input type="number" name="phone_number" class="form-control" required="required" placeholder="Fill Input Phone Number" value="{{ old('phone_number') != null ? old('phone_number') : $row->no_hp}}">
							</div>
							<div class="form-group">
								<label for="">Photo Profile</label>
								<img class="img-responsive" id="uploadPreview" alt="">
								<input type="file" name="photo_profile" class="form-control" id="image">
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-primary">
								Save
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection