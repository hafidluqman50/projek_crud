@extends('Administrator.AdminVideo.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Posts</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="{{ url('/admin-video/categories/create') }}">
							<button class="btn btn-primary">
								Create
							</button>
						</a>
					</div>
					<div class="box-body">
					    @if(session()->has('message'))
						<div class="alert alert-success alert-dismissible">
							{{ session('message') }} <button class="close" data-dismiss="alert">X</button>
						</div>
					    @endif
						<table class="table table-hover" id="categories" width="100%">
							<thead>
								<tr>
									<th>No.</th>
									<th>Name</th>
									<th>Icon</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection