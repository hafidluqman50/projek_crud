@extends('Administrator.Admin.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Posts Download</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="{{ url('/admin-video/posts') }}">
							<button class="btn btn-default">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="">Download File : </label>
							<a href="{{ url('/admin-video/posts/download/'.$id.'/data/file') }}">
								<button class="btn btn-success">
									Download
								</button>
							</a>
						</div>
						<div class="form-group">
							<label for="">Download File2 : </label>
							<a href="{{ url('/admin-video/posts/download/'.$id.'/data/file2') }}">
								<button class="btn btn-success">
									Download
								</button>
							</a>
						</div>
						<div class="form-group">
							<label for="">Download Audio : </label>
							<a href="{{ url('/admin-video/posts/download/'.$id.'/data/audio') }}">
								<button class="btn btn-success">
									Download
								</button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection