@extends('Administrator.AdminPhoto.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Users</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-body">
					    @if(session()->has('message'))
						<div class="alert alert-success alert-dismissible">
							{{ session('message') }} <button class="close" data-dismiss="alert">X</button>
						</div>
					    @endif
						<table class="table table-hover" id="users" width="100%">
							<thead>
								<tr>
									<th>No.</th>
									<th>Nick</th>
									<th>Username</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection