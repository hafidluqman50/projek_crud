@extends('Administrator.AdminPhoto.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Users Detail</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="{{ url('/admin-photo/users') }}">
							<button class="btn btn-default">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="">Nick</label>
							<input type="text" class="form-control" value="{{ $row->nick }}" readonly>
						</div>
						<div class="form-group">
							<label for="">Username</label>
							<input type="text" class="form-control" value="{{ $row->username }}" readonly>
						</div>
						<div class="form-group">
							<label for="">Age</label>
							<input type="text" class="form-control" value="{{ $row->umur }}" readonly>
						</div>
						<div class="form-group">
							<label for="">Gender</label>
							<input type="text" class="form-control" value="{{ ucwords($row->gender) }}" readonly>
						</div>
						<div class="form-group">
							<label for="">Bio</label>
							<textarea name="" id="" class="form-control" cols="30" rows="5" readonly>{{$row->bio}}</textarea>
						</div>
						<div class="form-group">
							<label for="">Birthday</label>
							<input type="text" class="form-control" value="{{ date_explode($row->tanggal_lahir) }}" readonly>
						</div>
						<div class="form-group">
							<label for="">Photo Profile</label>
							<img src="{{ asset('assets/photo_profile/'.$row->photo_profile) }}" class="img-responsive" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection