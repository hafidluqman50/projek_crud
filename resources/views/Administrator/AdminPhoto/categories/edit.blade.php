@extends('Administrator.AdminPhoto.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Categories Create</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="{{ url('/admin-photo/categories') }}">
							<button class="btn btn-default">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
					</div>
					<form action="{{ url('/admin-photo/categories/update/'.$id) }}" method="POST" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="box-body">
							<div class="form-group">
								<label for="">Name</label>
								<input type="text" name="name" class="form-control" value="{{ $row->name }}" required="required" placeholder="Fill Name Categories Input">
							</div>
							<div class="form-group">
								<label for="">Icon</label>
								<br>
								<img src="{{ asset('assets/icon_category/'.$row->icon) }}" style="width:10%; height:10%;">
								<input type="file" name="icon" class="form-control">
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-primary">
								Save
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection