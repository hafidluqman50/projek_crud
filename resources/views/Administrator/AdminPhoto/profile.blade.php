@extends('Administrator.Admin.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Profile</h1>		
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="box box-default">
					<div class="box-header with-border">
						<div class="col-md-6 col-md-offset-3">
							<img src="{{ asset('assets/admin_photo_profile/'.$row->foto) }}" class="img-responsive" alt="" width="100%" height="100%">
						</div>
					</div>
					<div class="box-body">
					    @if(session()->has('message'))
						<div class="alert alert-success alert-dismissible">
							{{ session('message') }} <button class="close" data-dismiss="alert">X</button>
						</div>
					    @endif
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" class="form-control" value="{{ $row->name }}" readonly>
						</div>
						<div class="form-group">
							<label for="">Username</label>
							<input type="text" class="form-control" value="{{ $row->username }}" readonly>
						</div>
						<div class="form-group">
							<label for="">Phone Number</label>
							<input type="text" class="form-control" value="{{ $row->no_hp }}" readonly>
						</div>
					</div>
					<div class="box-footer">
						<a href="{{ url('/admin-photo/profile/change') }}">
							<button class="btn btn-primary">
								Edit
							</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection