@extends('Administrator.AdminPhoto.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Posts Detail</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="{{ url('/admin-photo/posts') }}">
							<button class="btn btn-default">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
					</div>
					<div class="box-body">
						<div class="col-md-12">
							<div class="form-group">
								<img src="{{ asset('assets/file2/'.$row->file2) }}" class="img-responsive" alt="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Title</label>
								<input type="text" class="form-control" value="{{ $row->title }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Caption</label>
								<input type="text" class="form-control" value="{{ $row->caption }}" disabled>
							</div>
							<div class="form-group">
								<label for="">User</label>
								<input type="text" class="form-control" value="{{ $row->nick }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Width</label>
								<input type="text" class="form-control" value="{{ $row->width }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Height</label>
								<input type="text" class="form-control" value="{{ $row->height }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Type</label>
								<input type="text" class="form-control" value="{{ $row->type_post }}" disabled>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Like</label>
								<input type="text" class="form-control" value="{{ $row->like }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Views</label>
								<input type="text" class="form-control" value="{{ $row->views }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Comment</label>
								<input type="text" class="form-control" value="{{ $row->comment }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Report</label>
								<input type="text" class="form-control" value="{{ $row->report }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Category</label>
								<input type="text" class="form-control" value="{{ $row->name }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Longitude</label>
								<input type="text" class="form-control" value="{{ $row->longitude }}" disabled>
							</div>
							<div class="form-group">
								<label for="">Latitude</label>
								<input type="text" class="form-control" value="{{ $row->latitude }}" disabled>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection