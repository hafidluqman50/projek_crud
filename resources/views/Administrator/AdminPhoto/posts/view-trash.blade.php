@extends('Administrator.AdminPhoto.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Posts Trash</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="{{ url('/admin-photo/posts') }}">
							<button class="btn btn-default">
								<span class="fa fa-arrow-left"></span> Back
							</button>
						</a>
						<a href="{{ url('/admin-photo/posts/view-trash/delete-all') }}">
							<button class="btn btn-danger">
								Delete All
							</button>
						</a>
					</div>
					<div class="box-body">
						<table class="table table-hover" id="trash-posts" data-type="all" width="100%">
							<thead>
								<tr>
									<th>No.</th>
									<th>Title</th>
									<th>Caption</th>
									<th>Type</th>
									<th>User</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection