@extends('Administrator.AdminPhoto.layout.layout-app')

@section('content')
	<section class="content-header">
		<h1>Posts</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						{{-- <a href="{{ url('/admin-photo/posts/create') }}">
							<button class="btn btn-primary">
								Create
							</button>
						</a> --}}
						{{-- <a href="{{ url('/admin-photo/posts/view-trash') }}">
							<button class="btn btn-danger">
								View Trash
							</button>
						</a> --}}
					</div>
					<div class="box-body">
					    @if(session()->has('message'))
						<div class="alert alert-success alert-dismissible">
							{{ session('message') }} <button class="close" data-dismiss="alert">X</button>
						</div>
					    @endif
						<table class="table table-hover posts" type="photo" width="100%">
							<thead>
								<tr>
									<th>No.</th>
									<th>Title</th>
									<th>Caption</th>
									<th>Type</th>
									<th>User</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection