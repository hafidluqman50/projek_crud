// $(function(){
	function rupiah_format(string){
		if (Math.sign(string) !== -1) {
			reverse = string.toString().split('').reverse().join(''),
			ribuan 	= reverse.match(/\d{1,3}/g);
			ribuan	= 'Rp. '+ribuan.join('.').split('').reverse().join('')+',00';
			return ribuan;
		}
		return 'Rp. 0';
	}

	$("#image").change(function(){
		var file = document.getElementById("image").files[0];
		var readImg = new FileReader();
		readImg.readAsDataURL(file);
		readImg.onload = function(e) {
			$('#uploadPreview').attr('src',e.target.result).fadeIn();
		}
	});
// });

	$(function(){
		var getUrlHost = window.location.origin;
	    var urlSegment = window.location.pathname.split('/');
        console.log($('.posts').attr('type'));

        var posts = $('.posts').DataTable({
            processing:true,
            serverSide:true,
            ajax:getUrlHost+'/posts/datatables/'+$('.posts').attr('type'),
            columns:[
                {data:'post_id',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'title',name:'title'},
                {data:'caption',name:'caption'},
                {data:'type_post',name:'type_post'},
                {data:'nick',name:'nick'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        posts.on( 'order.dt search.dt', function () {
	        posts.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();

        var trash_posts = $('#trash-posts').DataTable({
            processing:true,
            serverSide:true,
            ajax:getUrlHost+'/posts/datatables/trash/'+$('#trash-posts').attr('data-type'),
            columns:[
                {data:'post_id',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'title',name:'title'},
                {data:'caption',name:'caption'},
                {data:'type_post',name:'type_post'},
                {data:'nick',name:'nick'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        trash_posts.on( 'order.dt search.dt', function () {
            trash_posts.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        var categories = $('#categories').DataTable({
            processing:true,
            serverSide:true,
            ajax:getUrlHost+'/categories/datatables',
            columns:[
                {data:'category_id',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'name',name:'name'},
                {data:'icon',name:'icon'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        categories.on( 'order.dt search.dt', function () {
            categories.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        var users = $('#users').DataTable({
            processing:true,
            serverSide:true,
            ajax:getUrlHost+'/users/datatables',
            columns:[
                {data:'user_id',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nick',name:'nick'},
                {data:'username',name:'username'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        users.on( 'order.dt search.dt', function () {
            users.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        var posts_guest = $('#posts-guest').DataTable({
            processing:true,
            serverSide:true,
            ajax:getUrlHost+'/posts-guest/datatables',
            columns:[
                {data:'post_id',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'title',name:'title'},
                {data:'caption',name:'caption'},
                {data:'type_post',name:'type_post'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        posts_guest.on( 'order.dt search.dt', function () {
            posts_guest.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        $('#check').click(function(){
            if ($(this).is(':checked')) {
                $('input[name="username"]').removeAttr('disabled');
            }
            else {
                $('input[name="username"]').attr('disabled','disabled');
            }
        });
	});

// $(function(){
// 	$('body').on('keydown','input,select,textarea',function(e){
// 		var self = $(this),
// 			form = self.parents('form:eq(0)'),
// 			focusable,
// 			next
// 			;
// 		if (e.keyCode == 13) {
// 			focusable = form.find('input,a,select,button,textarea').filter(':visible');
// 			console.log(focusable);
// 			next = focusable.eq(focusable.index(this)+1);
// 			if (next.length) {
// 				next.focus();
// 			}
// 			else {
// 				next.submit();
// 			}
// 			return false;
// 		}
// 	});
	
// 	$('#sip').click(function(){
// 		if ($(this).is(':checked')) {
// 			$('input[name="username"]').removeAttr('disabled');
// 		}
// 		else {
// 			$('input[name="username"]').attr('disabled','disabled');
// 		}
// 	});
// });